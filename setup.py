#!/usr/bin/env python

from distutils.core import setup

setup(name='wlvlang',
      version='0.0',
      description='Programming Language created using RPython',
      author='Samuel Giles',
      author_email='s.giles@wlv.ac.uk',
      license='MIT',
      url='',
      packages=['wlvlang', 'wlvlang.compiler'],
     )
